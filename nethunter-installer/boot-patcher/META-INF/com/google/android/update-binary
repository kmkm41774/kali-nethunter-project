#!/sbin/sh
# NetHunter kernel installer backend

## start build generated variables
generic="arm64"
## end build generated variables

if [ "$3" ]; then
	zip=$3
	console=/proc/$$/fd/$2
	# write the location of the console buffer to /tmp/console for other scripts to use
	echo "$console" > /tmp/console
else
	console=$(cat /tmp/console)
	[ "$console" ] || console=/proc/$$/fd/1
fi

test "$ANDROID_ROOT" || ANDROID_ROOT=/system
SYSTEM="/system"

tmp=/tmp/nethunter/boot-patcher
print() {
	echo "ui_print ${1:- }" > "$console"
	echo
}

abort() {
	[ "$1" ] && {
		print "Error: $1"
		print "Aborting..."
	}
	cleanup
	print "Failed to patch boot image!"
	exit 1
}

#reference: https://github.com/osm0sis/AnyKernel3/blob/master/META-INF/com/google/android/update-binary#L33+L40
setup_mountpoint() {
  test -L $1 && $BB mv -f $1 ${1}_link;
  if [ ! -d $1 ]; then
    rm -f $1;
    mkdir -p $1;
  fi;
}
is_mounted() { $BB mount | $BB grep -q " $1 "; }

#for reference: https://github.com/osm0sis/AnyKernel3/blob/master/META-INF/com/google/android/update-binary#L91+L127
mount_all() {
  if ! is_mounted /data; then
    $BB mount /data;
    UMOUNT_DATA=1;
  fi;
  setup_mountpoint $ANDROID_ROOT;
  if ! is_mounted $ANDROID_ROOT; then
    $BB mount -o rw -t auto $ANDROID_ROOT 2>/dev/null;
  fi;
  case $ANDROID_ROOT in
    /system_root) setup_mountpoint /system;;
    /system)
      if ! is_mounted /system && ! is_mounted /system_root; then
        setup_mountpoint /system_root;
        $BB mount -o rw -t auto /system_root;
      elif [ -f /system/system/build.prop ]; then
        setup_mountpoint /system_root;
        $BB mount --move /system /system_root;
      fi;
      if [ $? != 0 ]; then
        $BB umount /system;
        $BB umount -l /system 2>/dev/null;
        test -e /dev/block/bootdevice/by-name/system || local slot=$(getprop ro.boot.slot_suffix 2>/dev/null);
        $BB mount -o rw -t auto /dev/block/bootdevice/by-name/system$slot /system_root;
      fi;
    ;;
  esac;
  if is_mounted /system_root; then
    if [ -f /system_root/build.prop ]; then
      $BB mount -o bind /system_root /system;
    else
      $BB mount -o bind /system_root/system /system;
    fi;
  fi;
}

cleanup() {
       if [ "$zip" ]; then
       rm /tmp/console
       cd $(dirname $tmp)
       rm -rf $tmp
       fi
}

install() {
	setperm "$2" "$3" "$tmp$1"
	if [ "$4" ]; then
		cp -r "$tmp$1" "$(dirname "$4")/"
		return
	fi
	cp -r "$tmp$1" "$(dirname "$1")/"
}

extract() {
	rm -rf "$2"
	mkdir -p "$2"
	unzip -o "$1" -d "$2" ||
		abort "Unable to extract! The zip may be corrupt or your device may not have enough RAM to proceed. Consider using a smaller installer if it is available."
}

setperm() {
	find "$3" -type d -exec chmod "$1" {} \;
	find "$3" -type f -exec chmod "$2" {} \;
}

get_bb() {
    cd $tmp/tools
    BB_latest=`(ls -v busybox_nh-* 2>/dev/null || ls busybox_nh-*) | tail -n 1`
    BB=$tmp/tools/$BB_latest #Use NetHunter Busybox from tools
    chmod 755 $BB #make busybox executable
    echo $BB
    cd - >/dev/null
}


install_recovery_bb() {
    # To identify the latest busybox version, we need the command "ls -v" which is not supported by all twrp versions
    # We will pick the best available busybox version and use that to run "ls -v" to pick the latest version for the recovery partition
    cd $tmp/tools

    # Try to pick the latest version of busybox, if "ls -v" is not supported by the recovery then make an educated guess for a later version
    recovery_bb=`(ls -v busybox_nh-* 2>/dev/null || ls busybox_nh-*) | tail -n 1`
    cp $recovery_bb busybox_tmp
    setperm 0755 0755 busybox_tmp

    # Now that we picked a reasonably current busybox, use that to copy the absolute latest to the recovery partition using "ls -v"
    nethunter_bb=$(./busybox_tmp ls -v busybox_nh-* | tail -n 1)
    print "Installing $nethunter_bb applets to /sbin"
    cp $nethunter_bb /sbin/busybox_nh
    /sbin/busybox_nh --install /sbin
    cd - >/dev/null
}

if [ "$zip" ]; then
print "##################################################"
print "##                                              ##"
print "##  88      a8P         db        88        88  ##"
print "##  88    .88'         d88b       88        88  ##"
print "##  88   88'          d8''8b      88        88  ##"
print "##  88 d88           d8'  '8b     88        88  ##"
print "##  8888'88.        d8YaaaaY8b    88        88  ##"
print "##  88P   Y8b      d8''''''''8b   88        88  ##"
print "##  88     '88.   d8'        '8b  88        88  ##"
print "##  88       Y8b d8'          '8b 888888888 88  ##"
print "##                                              ##"
print "####  ############# NetHunter ####################"

else 

print "##################################################"
print "          A/B and A Devices Boot-patcher"
if [ "$generic" ]; then
	print "  for $generic devices - ramdisk modifications only"
fi
print "##################################################"
fi

# Unpack the installer
[ "$zip" ] && {
	print "- Unpacking the installer, this may take a while..."
	extract "$zip" "$tmp"
	print "- Installer unpacked"
}
cd "$tmp"

BB=$(get_bb)

chmod +x env
. ./env.sh
# Install busybox applets if they haven't been installed by the NetHunter installer already
[ -e /tmp/nethunter/tools/busybox_tmp ] || install_recovery_bb

#we need to mount system here as it is disabled in update-binary-anykernel
print "- Mounting partitions for Boot-Patching"
mount_all;
print "- Partitions mounted"
setperm 0755 0755 tools


print "- Running boot image patcher..."
sh "META-INF/com/google/android/update-binary-anykernel" || abort

print "- Boot image patching complete"
cleanup
